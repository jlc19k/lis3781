> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Managment

## Jason Choate

### Assignment #3 Requirements:

*Required:*

1. Log into Oracle Server using RemoteLabs
2. Create and populate Oracle tables

#### README.md file should include the following items:

* Screenshot of my SQL code
* Screenshot of my populated tables in Oracle enviroment
* My LIS3781 Bitbucket repo link


#### Assignment Screenshots:

*Screenshot of SQL code*:

![My SQL code](img/sql_code2.PNG)

*Screenshot of populated customer table*:

![Populated customer table](img/customer_table.PNG)

*Screenshot of populated commodity table*:

![Populated commodity table](img/commodity_table.PNG)

*Screenshot of populated order table*:

![Populated order table](img/order_table.PNG)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jlc19k/bitbucketstationlocations/ "Bitbucket Station Locations")

*My LIS3781 Bitbucket repo link:*
[My LIS3781 repo](https://bitbucket.org/jlc19k/lis3781/ "My LIS3781 repo")
