> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Managment

## Jason Choate

### Project #2 Requirements:

*Required:*

1. Install and set up MongoDB and Atlas
2. Insert primer-dataset.json
3. Answer MongoDB questions

#### README.md file should include the following items:

* Screenshot of at least one MongoDB shell command
* Optional: JSON code for required reports
* Bitbucket repo links


#### Assignment Screenshots:

*Screenshot of show collections command*:

![show collections command](https://bitbucket.org/jlc19k/lis3781/raw/f99d2726a52f0499f1c81c2c80139006f237b802/p2/img/mongo_cmd_1.PNG)

*Screenshot of db.restaurants.count() command*:

![db.restaurants.count() command](https://bitbucket.org/jlc19k/lis3781/raw/f99d2726a52f0499f1c81c2c80139006f237b802/p2/img/mongo_cmd_2.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jlc19k/bitbucketstationlocations/ "Bitbucket Station Locations")

*My LIS3781 Bitbucket repo link:*
[My LIS3781 repo](https://bitbucket.org/jlc19k/lis3781/ "My LIS3781 repo")
