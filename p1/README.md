> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Managment

## Jason Choate

### Project #1 Requirements:

*Required:*

1. Create and populate ERD
2. Answer SQL Statement questions


#### README.md file should include the following items:

* Screenshot of ERD
* Optional: SQL code for required reports.
* Bitbucket repo links.


#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/P1ERD.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jlc19k/bitbucketstationlocations/ "Bitbucket Station Locations")

*My LIS3781 Bitbucket repo link:*
[My LIS3781 repo](https://bitbucket.org/jlc19k/lis3781/ "My LIS3781 repo")
