> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Jason Choate

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install and configure git and bitbucket
    - Install AMPPS
    - Provide git command descriptions
    - Entity Relationship Diagram
    - Push local repository to Bitbucket
    - Provide read-only access to Bitbucket repository

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create company and customer tables
    - Create insert statements to populate tables

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Log into Oracle Server using RemoteLabs
    - Create and populate Oracle tables

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create and populate ERD
    - Answer SQL Statement questions

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create and populate database using MS SQL Server
    - Answer SQL Statement questions

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Add and populate required tables using MS SQL Server
    - Answer SQL Statement questions

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install and set up MongoDB and Atlas
    - Insert primer-dataset.json
    - Answer MongoDB questions