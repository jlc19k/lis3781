> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Managment

## Jason Choate

### Assignment #1  Requirements:

*Six Parts:*

1. Install and configure git and bitbucket
2. Install AMPPS
3. Provide git command descriptions
4. Entity Relationship Diagram
5. Push local repository to Bitbucket
6. Provide read-only access to Bitbucket repository

#### README.md file should include the following items:

* Screenshot of AMPPS installation running
* git commands with short descriptions
* My ERD image
* Bitbucket repo links for both this assignment and tutorial repo (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - "initializes" a new git repository.
2. git status - shows the current state of the working directory, including what changes have been made and are awaiting a commit.
3. git add - takes a change in the working directory and adds it to the staging area to be updated during the next commit.
4. git commit - records and pushes changes to files within a repository to the main repository.
5. git push - uploads a local repository and its contents to a remote repository.
6. git pull - obtains content from a remote repositiory 
7. git clone - clones a repository into a newly created directory, initializing it as a new repository.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of Entity Relationship Diagram*:

![Entity Relationship Diagram](img/jdk_install.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
