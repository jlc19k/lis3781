> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Managment

## Jason Choate

### Assignment #2 Requirements:

*Required:*

1. Create company and customer tables
2. Create insert statements to populate tables

#### README.md file should include the following items:

* Screenshot of SQL statements used to create and populate tables
* Screenshot of SQL statements to show populated tables

#### Assignment Screenshots:

*Screenshot SQL statements for Company table*:
![Company Table Generation Screenshot](https://bitbucket.org/jlc19k/lis3781/raw/89c199a25f41158bbd1053da5ace53027acfa820/a2/img/sql_company_table.PNG)

*Screenshot of SQL statements for Customer table*:
![Customer Table Generation Screenshot](https://bitbucket.org/jlc19k/lis3781/raw/89c199a25f41158bbd1053da5ace53027acfa820/a2/img/sql_customer_table.PNG)

*Screenshot of populated company table*:
![Populated Company Table Screenshot](https://bitbucket.org/jlc19k/lis3781/raw/89c199a25f41158bbd1053da5ace53027acfa820/a2/img/company_populated.PNG)

*Screenshot of populated customer table*:
![Populated Customer Table Screenshot](https://bitbucket.org/jlc19k/lis3781/raw/89c199a25f41158bbd1053da5ace53027acfa820/a2/img/customer_populated.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jlc19k/bitbucketstationlocations/ "Bitbucket Station Locations")
