> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Managment

## Jason Choate

### Assignment #5 Requirements:

*Required:*

1. Log into Microsoft SQL Server
2. Add and populate new tables

#### README.md file should include the following items:

* Screenshot of my ERD
* OPTIONAL: SQL code for required reports
* My LIS3781 Bitbucket repo link


#### Assignment Screenshots:

*Screenshot of my ERD*:

![ERD Screenshot](img/A5_ERD.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jlc19k/bitbucketstationlocations/ "Bitbucket Station Locations")

*My LIS3781 Bitbucket repo link:*
[My LIS3781 repo](https://bitbucket.org/jlc19k/lis3781/ "My LIS3781 repo")

